import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('E41201367 - Fauzan Abdillah'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            text(),
            icon(),
            button(),
            textformfield(),
          ],
        ),
      ),
    );
  }

  Widget text() {
    return const Center(
      child: Text(
        "Ini Text Widget",
        style: TextStyle(
          color: Colors.blue,
          backgroundColor: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget icon() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: const [
              Icon(Icons.access_alarm),
              Text('Alarm'),
            ],
          ),
          Column(
            children: const [
              Icon(Icons.phone),
              Text('Phone'),
            ],
          ),
          Column(
            children: const [
              Icon(Icons.book),
              Text('Book'),
            ],
          ),
        ],
      ),
    );
  }

  Widget button() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
            color: Colors.amber,
            child: const Text("Ini Raised Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          MaterialButton(
            color: Colors.lime,
            child: const Text("Ini Material Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          FlatButton(
            color: Colors.lightGreenAccent,
            child: const Text("Ini FlatButton Button"),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget textformfield() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 40,
        vertical: 10,
      ),
      child: Form(
        child: Column(
          children: [
            TextFormField(
              decoration: const InputDecoration(hintText: "Username"),
            ),
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(hintText: "Password"),
            ),
            const SizedBox(height: 30),
            RaisedButton(
              child: const Text("Login"),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
  }
}