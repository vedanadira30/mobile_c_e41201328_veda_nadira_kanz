import 'package:flutter/material.dart';
import 'package:flutter_styling/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
