import 'dart:io';

void main() {
  print("=== WEREWOLF GAME ===");

  stdout.write("enter your username : ");
  var userName = stdin.readLineSync();
  if (userName == "") {
    print("Fill your username!");
  }

  stdout.write("Hello $userName enter your role : ");
  var userRole = stdin.readLineSync();
  if (userRole == "") {
    print("Fill your role!");
  }

  if (userRole == "" || userName == "") {
    print("Data can't be empty!");
  } else if (userRole == "Wizard") {
    print(
        "Welcome to Werewolf Wordl! Hello $userRole $userName, you can see who is the Werewolf!");
  } else if (userRole == "Guard") {
    print(
        "Welcome to Werewolf Wordl! Hello $userRole $userName, you can protect your friends from werewolf attack!");
  } else if (userRole == "Werewolf") {
    print(
        "Welcome to Werewolf Wordl! Hello $userRole $userName, you can eat your prey each night!");
  } else {
    print("Fill with the right role");
  }
}
