import 'dart:io';

void main() {
  print("===== Veda's Quotes Qorner =====");

  stdout.write("Choose your day : ");
  String? day = stdin.readLineSync()?.trim().toLowerCase();

  String quote;

  switch (day) {
    case "sunday":
      {
        quote =
            "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.";
        break;
      }
    case "monday":
      {
        quote =
            "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.";
        break;
      }
    case "tuesday":
      {
        quote =
            "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.";
        break;
      }
    case "wednesday":
      {
        quote =
            "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.";
        break;
      }
    case "thursday":
      {
        quote =
            "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.";
        break;
      }
    case "friday":
      {
        quote = "Hidup tak selamanya tentang kekasih";
        break;
      }
    case "saturday":
      {
        quote = "Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.";
        break;
      }
    default:
      {
        quote = "You input the wrong day!";
      }
  }
  print(quote);
}
