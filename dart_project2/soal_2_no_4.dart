import 'dart:io';

void main() {
  var tanggal = 2;
  var bulan = 2;
  var tahun = 1980;

  String output;

  if (tanggal >= 1 && tanggal <= 31 && tahun >= 1900 && tahun <= 2200) {
    switch (bulan) {
      case 1:
        {
          output = "$tanggal Januari $tahun";
          break;
        }
      case 2:
        {
          output = "$tanggal Februari $tahun";
          break;
        }
      case 3:
        {
          output = "$tanggal Maret $tahun";
          break;
        }
      case 4:
        {
          output = "$tanggal April $tahun";
          break;
        }
      case 5:
        {
          output = "$tanggal Mei $tahun";
          break;
        }
      case 6:
        {
          output = "$tanggal Juni $tahun";
          break;
        }
      case 7:
        {
          output = "$tanggal Juli $tahun";
          break;
        }
      case 8:
        {
          output = "$tanggal Agustus $tahun";
          break;
        }
      case 9:
        {
          output = "$tanggal September $tahun";
          break;
        }
      case 10:
        {
          output = "$tanggal Oktober $tahun";
          break;
        }
      case 11:
        {
          output = "$tanggal November $tahun";
          break;
        }
      case 12:
        {
          output = "$tanggal Desember $tahun";
          break;
        }
      default:
        {
          output = "Format tanggal tidak valid!";
        }
    }
    print(output);
  } else {
    print("Format data tidak valid!");
  }
}
