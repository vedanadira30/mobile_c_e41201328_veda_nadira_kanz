import 'dart:async';

void main() {
  print('Diri - Tulus');

  Future.delayed(
    Duration(seconds: 1),
    () => print('Mari berdamai dengan diri kita sendiri （＾∀＾)'),
  );
  Future.delayed(
    Duration(seconds: 2),
    () => print('Hari ini'),
  );
  Future.delayed(
    Duration(seconds: 4),
    () => print('Kau berdamai dengan dirimu sendiri'),
  );
  Future.delayed(
    Duration(seconds: 11),
    () => print('Kau maafkan'),
  );
  Future.delayed(
    Duration(seconds: 13),
    () => print('Semua salahmu, ampuni dirimu'),
  );
  Future.delayed(
    Duration(seconds: 22),
    () => print('Hari ini'),
  );
  Future.delayed(
    Duration(seconds: 24),
    () => print('Ajak lagi dirimu bicara mesra'),
  );
  Future.delayed(
    Duration(seconds: 31),
    () => print('Berjujurlah'),
  );
  Future.delayed(
    Duration(seconds: 34),
    () => print('Pada dirimu kau bisa percaya'),
  );
}
